#! /bin/bash

# Variables
build_directory=drupal
drush=$(pwd)/$build_directory/vendor/bin/drush
repo_parent=$(pwd)/../..

echo "Setting up the Drupal site..."
cp config/settings.php $build_directory/web/sites/default/.
cp config/settings.local.php $build_directory/web/sites/default/.

pushd $build_directory/web;
$drush site:install minimal -y -vvv \
    --verbose \
    --site-name=Drupal CI Sandbox \
    --account-name=drupal \
    --account-pass=drupal \
    --account-mail=mail@example.com

# Need to get the vitals module to where it needs to be.
rsync -av --exclude=".*" $repo_parent/vitals modules
$drush en vitals -y
$drush cr
pushd
