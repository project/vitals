#! /bin/bash

# Variables
build_directory=drupal
drush=$(pwd)/$build_directory/vendor/bin/drush

echo "Starting tests..."
# Starting the built in webserver.
# php -S localhost:8080 -t $build_directory/web &
$drush runserver http://localhost:8080 > /dev/null 2>&1 &

# Waiting just a bit before hitting it with the tests.
sleep 5

pushd $build_directory/web/core;
# Run only the Vitals tests. Using verbose output for convienence. Build should
# fail if there are any errors.
php ./scripts/run-tests.sh --sqlite /tmp/vitals.sqlite --concurrency 1 --url http://localhost:8080 --verbose Vitals
test_exit_code=$?
popd

exit $test_exit_code
