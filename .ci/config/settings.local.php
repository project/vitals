<?php

/**
 * @file
 * Local settings for GitHub Actions.
 */

$databases['default']['default'] = [
  'database' => 'ci',
  'username' => 'ci',
  'password' => 'ci',
  'host' => '127.0.0.1',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
];
