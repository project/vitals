<?php

namespace Drupal\Tests\vitals\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Core\Url;
use Drupal\vitals\Vitals;

/**
 * Class VitalsTest.
 *
 * Functional tests for the Vitals module.
 *
 * @group Vitals
 */
class VitalsTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * This array of modules will be enabled when the fixture Drupal site is
   * built. We leave it empty here because this is a skeleton test. A typical
   * test will enable basic modules like node and user.
   *
   * @var string[]
   */
  protected static $modules = ['vitals'];

  /**
   * The default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($user);
  }

  /**
   * Tests happy path scenerio.
   */
  public function testVitalsSuccess() {
    $state = $this->container->get('state');
    $token = $state->get('vitals.token');
    $url = Url::fromRoute('vitals.content', ['token' => $token])->toString();

    $this->drupalGet($url);

    // Checking for the keys we know should exist.
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('php_version');
    $this->assertSession()->responseContains('cms_version');
    $this->assertSession()->responseContains('security_updates');
    $this->assertSession()->responseContains('pending_updates');
    $this->assertSession()->responseContains('themes');
    $this->assertSession()->responseContains('stark');
  }

  /**
   * Tests plugin system by disabling a check.
   */
  public function testVitalsPluginSystem() {
    $config = $this->container->get('config.factory')->getEditable('vitals.settings');
    $enabled_plugins = [
      'cms_version' => 'cms_version',
      'php_version' => 'php_version',
      'themes' => 'themes',
      'updates' => 0,
    ];
    $config->set('vitals_enabled_plugins', $enabled_plugins)->save();

    $state = $this->container->get('state');
    $token = $state->get('vitals.token');
    $url = Url::fromRoute('vitals.content', ['token' => $token])->toString();

    $this->drupalGet($url);

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('php_version');
    $this->assertSession()->responseContains('cms_version');
    $this->assertSession()->responseContains('themes');
    $this->assertSession()->responseNotContains('security_updates');
    $this->assertSession()->responseNotContains('pending_updates');
  }

  /**
   * Tests 404 configuration and response.
   */
  public function testVitals404() {
    $config = $this->container->get('config.factory')->getEditable('vitals.settings');
    $config->set('vitals_unauthorized_action', Vitals::VITALS_404)->save();

    $this->drupalGet('vitals/invalid-token');
    $this->assertSession()->statusCodeEquals(404);
  }

  /**
   * Tests flood prevention (403)
   */
  public function testFloodPrevention403() {
    $config = $this->container->get('config.factory')->getEditable('vitals.settings');
    $config->set('vitals_unauthorized_action', Vitals::VITALS_403)->save();

    // Simulate and check several invalid requests.
    for ($i = 0; $i < 11; $i++) {
      $this->drupalGet('vitals/invalid-token');
      $this->assertSession()->statusCodeEquals(403);
    }

    // Simulate a valid request and ensure it is rejected.
    $state = $this->container->get('state');
    $token = $state->get('vitals.token');
    $url = Url::fromRoute('vitals.content', ['token' => $token])->toString();

    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests flood prevention (404)
   */
  public function testFloodPrevention404() {
    $config = $this->container->get('config.factory')->getEditable('vitals.settings');
    $config->set('vitals_unauthorized_action', Vitals::VITALS_404)->save();

    // Simulate and check several invalid requests.
    for ($i = 0; $i < 11; $i++) {
      $this->drupalGet('vitals/invalid-token');
      $this->assertSession()->statusCodeEquals(404);
    }

    // Simulate a valid request and ensure it is rejected.
    $state = $this->container->get('state');
    $token = $state->get('vitals.token');
    $url = Url::fromRoute('vitals.content', ['token' => $token])->toString();

    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(404);
  }

  /**
   * Tests partial flood prevention.
   */
  public function testPartialFloodPrevention() {
    $config = $this->container->get('config.factory')->getEditable('vitals.settings');
    $config->set('vitals_unauthorized_action', Vitals::VITALS_404)->save();

    // Simulate and check several invalid requests.
    for ($i = 0; $i < 5; $i++) {
      $this->drupalGet('vitals/invalid-token');
      $this->assertSession()->statusCodeEquals(404);
    }

    // Simulate a valid request and ensure it is rejected.
    $state = $this->container->get('state');
    $token = $state->get('vitals.token');
    $url = Url::fromRoute('vitals.content', ['token' => $token])->toString();

    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);
  }

}
