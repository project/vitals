<?php

namespace Drupal\vitals;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class VitalsGetData.
 *
 * @package Drupal\vitals
 */
class Vitals {

  /**
   * The 403 HTTP Response Code.
   */
  const VITALS_403 = 403;

  /**
   * The 404 HTTP Response Code.
   */
  const VITALS_404 = 404;

  /**
   * The plugin manager for the vitals checks.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $plugin_manager;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a new Vitals object.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The plugin manager for the vitals checks.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(
    PluginManagerInterface $plugin_manager,
    ConfigFactoryInterface $config_factory
  ) {
    $this->plugin_manager = $plugin_manager;
    $this->config = $config_factory->get('vitals.settings');
  }

  /**
   * Gets a generated authentication token.
   */
  public function generateToken() {
    return bin2hex(random_bytes(64));
  }

  /**
   * Gets site status.
   */
  public function getStatus() {
    $info = [];

    $enabled_checks = $this->getEnabledChecks();

    foreach ($enabled_checks as $plugin_id) {
      $check = $this->plugin_manager->createInstance($plugin_id);
      $info[$plugin_id] = $check->getData();
    }

    return $info;
  }

  /**
   * Helper function to get only the enabled plugins.
   */
  private function getEnabledChecks() {
    $enabled_checks = [];
    $config_enabled = $this->config->get('vitals_enabled_plugins');

    foreach ($config_enabled as $plugin_id => $status) {
      if ($plugin_id === $status) {
        $enabled_checks[] = $plugin_id;
      }
    }

    return $enabled_checks;
  }

}
