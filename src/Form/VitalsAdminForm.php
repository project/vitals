<?php

namespace Drupal\vitals\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\vitals\Vitals;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the configuration form for Vitals.
 */
class VitalsAdminForm extends ConfigFormBase {

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The the state object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Include the messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The object that gets the data.
   *
   * @var \Drupal\vitals
   */
  protected $vitals;

  /**
   * The plugin manager for the vitals checks.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $plugin_manager;

  /**
   * Constructs a new VitalsAdminForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\vitals\Vitals $vitals
   *   The object that gets the data.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The plugin manager for the vitals checks.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    StateInterface $state,
    MessengerInterface $messenger,
    Vitals $vitals,
    PluginManagerInterface $plugin_manager
  ) {
    $this->config = $config_factory->getEditable('vitals.settings');
    $this->state = $state;
    $this->messenger = $messenger;
    $this->vitals = $vitals;
    $this->plugin_manager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('messenger'),
      $container->get('vitals'),
      $container->get('plugin.manager.vitals_check')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vitals.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vitals_form';
  }

  /**
   * Creates the configuration form for the Vitals module.
   */
  public function generateNewToken($form, FormStateInterface $form_state) {
    $token = $this->vitals->generateToken();
    $this->state->set('vitals.token', $token);
    $this->messenger->addMessage($this->t('A new token has been generated.'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $token = $this->state->get('vitals.token');

    $form['#attached']['library'][] = 'vitals/vitals_admin';

    $form['vitals_token'] = [
      '#type' => 'hidden',
      '#value' => $this->state->get('vitals.token'),
      '#attributes' => [
        'id' => 'js-hidden-token',
      ],
    ];

    $form['vitals_copy_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Copy Current Token'),
      '#attributes' => [
        'id' => 'js-copy-button',
      ],
    ];

    $form['vitals_generate_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate New Token'),
      '#attributes' => [
        'id' => 'js-generate-button',
      ],
      '#submit' => ['::generateNewToken'],
    ];

    $form['vitals_unauthorized_action'] = [
      '#type' => 'select',
      '#title' => $this->t('Unauthorized Action'),
      '#default_value' => $this->config->get('vitals_unauthorized_action'),
      '#description' => $this->t('This allows you to change the behavior if the endpoint is used without a valid token.'),
      '#options' => [
        Vitals::VITALS_403 => $this->t('403 - Forbidden'),
        Vitals::VITALS_404 => $this->t('404 - Not Found'),
      ],
    ];

    $form['vitals_enabled_plugins'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled Vitals Checks'),
      '#default_value' => $this->config->get('vitals_enabled_plugins'),
      '#description' => $this->t('Choose which vitals checks that you want to appear on the vitals endpoint.'),
      '#options' => $this->getVitalsCheckPluginsOptions(),
    ];

    $form['vitals_link'] = [
      '#markup' => $this->t('You can hit the vitals end point by clicking <a href="@url" target="_blank">here</a>.', [
        '@url' => Url::fromRoute('vitals.content', ['token' => $token])->toString(),
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config
      ->set('vitals_unauthorized_action', $form_state->getValue('vitals_unauthorized_action'))
      ->set('vitals_enabled_plugins', $form_state->getValue('vitals_enabled_plugins'))
      ->save();
  }

  /**
   * Helper method to build the #options for the enabled plugins.
   */
  private function getVitalsCheckPluginsOptions() {
    $options = [];

    foreach ($this->plugin_manager->getDefinitions() as $definition) {
      $plugin_id = $definition['id'];
      $description = $definition['label'] . ' &dash; ' . $definition['description'];
      $options[$plugin_id] = $description;
    }

    ksort($options);

    return $options;
  }

}
