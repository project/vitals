<?php

namespace Drupal\vitals\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\vitals\Vitals;

/**
 * Output the status info of this site.
 */
class VitalsController extends ControllerBase {

  /**
   * The config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The the state object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The object that gets the data.
   *
   * @var \Drupal\vitals\Vitals
   */
  protected $vitals;

  /**
   * Constructs a new VitalsController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\vitals\Vitals $vitals
   *   The object that gets the data.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    StateInterface $state,
    FloodInterface $flood,
    LoggerChannelFactoryInterface $logger_factory,
    RequestStack $request_stack,
    Vitals $vitals
  ) {
    $this->config = $config_factory->get('vitals.settings');
    $this->state = $state;
    $this->flood = $flood;
    $this->logger = $logger_factory->get('vitals');
    $this->requestStack = $request_stack;
    $this->vitals = $vitals;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('flood'),
      $container->get('logger.factory'),
      $container->get('request_stack'),
      $container->get('vitals')
    );
  }

  /**
   * Display the page.
   */
  public function output($token) {
    // Use the flood api to prevent brute force attacks. Limits access by IP
    // address to 10 attempts per hour.
    if (!$this->flood->isAllowed('vitals_check_attempt', 10)) {
      $ip = $this->requestStack->getCurrentRequest()->getClientIp();
      $this->logger->warning('Flood prevention has been triggered. It is possible that an attacker at @ip is targeting your site. If the problem persists, consider banning that IP address.', ['@ip' => $ip]);
      $this->sendUnauthorizedResponse();
    }

    // Compare the tokens using hash_equals to prevent timing attacks.
    $known_token = $this->state->get('vitals.token');
    if (!hash_equals($known_token, $token)) {
      $this->logger->warning('Invalid access token. Check to make sure any monitoring services are using the correct token.');

      // Register the flood event for invalid attempts.
      $this->flood->register('vitals_check_attempt');

      $this->sendUnauthorizedResponse();
    }

    // Clear flood prevention check on success.
    $this->flood->clear('vitals_check_attempt');

    $status = $this->vitals->getStatus();
    return new JsonResponse($status);
  }

  /**
   * Sends the configured unauthorized response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Exception thrown when Vitals is configured to send 404 responses.
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Exception thrown when Vitals is configured to send 403 responses.
   */
  private function sendUnauthorizedResponse() {
    $unauth_action = $this->config->get('vitals_unauthorized_action');

    if ($unauth_action == Vitals::VITALS_404) {
      throw new NotFoundHttpException();
    }
    else {
      throw new AccessDeniedHttpException();
    }
  }

}
