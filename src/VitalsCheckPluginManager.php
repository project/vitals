<?php

namespace Drupal\vitals;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * VitalsCheck plugin manager.
 */
class VitalsCheckPluginManager extends DefaultPluginManager {

  /**
   * Constructs VitalsCheckPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/VitalsCheck',
      $namespaces,
      $module_handler,
      'Drupal\vitals\VitalsCheckInterface',
      'Drupal\vitals\Annotation\VitalsCheck'
    );
    $this->alterInfo('vitals_check_info');
    $this->setCacheBackend($cache_backend, 'vitals_check_plugins');
  }

}
