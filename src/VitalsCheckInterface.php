<?php

namespace Drupal\vitals;

/**
 * Interface for vitals_check plugins.
 */
interface VitalsCheckInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the data that is being inspected by this vitals check.
   *
   * @return mixed
   *   The data that is being inspected by this vitals check.
   */
  public function getData();

}
