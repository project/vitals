<?php

namespace Drupal\vitals;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for vitals_check plugins.
 */
abstract class VitalsCheckPluginBase extends PluginBase implements VitalsCheckInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
