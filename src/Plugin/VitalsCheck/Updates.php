<?php

namespace Drupal\vitals\Plugin\VitalsCheck;

use Drupal\update\UpdateManagerInterface;
use Drupal\vitals\VitalsCheckPluginBase;

/**
 * Plugin implementation of the vitals_check for checking for pending updates.
 *
 * @VitalsCheck(
 *   id = "updates",
 *   label = @Translation("Updates"),
 *   description = @Translation("Lists of pending updates (both security and feature).")
 * )
 */
class Updates extends VitalsCheckPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return $this->getUpdates();
  }

  /**
   * Gets version data for installed modules.
   */
  private function getExtensions() {
    $extensions = [];

    if ($available = update_get_available(TRUE)) {
      \Drupal::moduleHandler()->loadInclude('update', 'inc', 'update.compare');
      $extensions = update_calculate_project_data($available);
    }

    return $extensions;
  }

  /**
   * Gets version data for installed modules.
   */
  private function getUpdates() {
    $extensions = $this->getExtensions();

    $updates = [];
    $security_updates = [];

    foreach ($extensions as $extension) {
      $status = $extension['status'];
      $machine_name = $extension['name'];
      $recommended_version = $extension['recommended'];

      switch ($status) {
        case UpdateManagerInterface::NOT_SECURE:
        case UpdateManagerInterface::REVOKED:
        case UpdateManagerInterface::NOT_SUPPORTED:
          $security_updates[$machine_name] = $recommended_version;
          $updates[$machine_name] = $recommended_version;
          break;

        case UpdateManagerInterface::NOT_CURRENT:
          $updates[$machine_name] = $recommended_version;
          break;
      }
    }

    $update_data = [
      'pending_updates' => $updates,
      'security_updates' => $security_updates,
    ];

    return $update_data;
  }

}
