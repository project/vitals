<?php

namespace Drupal\vitals\Plugin\VitalsCheck;

use Drupal\vitals\VitalsCheckPluginBase;

/**
 * Plugin implementation of the vitals_check for checking the active default and admin themes.
 *
 * @VitalsCheck(
 *   id = "themes",
 *   label = @Translation("Themes"),
 *   description = @Translation("Currently active default and admin themes")
 * )
 */
class Themes extends VitalsCheckPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $themes = [
      'default_theme' => \Drupal::config('system.theme')->get('default'),
      'admin_theme' => \Drupal::config('system.theme')->get('admin'),
    ];

    return $themes;
  }

}
