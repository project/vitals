<?php

namespace Drupal\vitals\Plugin\VitalsCheck;

use Drupal\vitals\VitalsCheckPluginBase;

/**
 * Plugin implementation of the vitals_check for checking the PHP version.
 *
 * @VitalsCheck(
 *   id = "php_version",
 *   label = @Translation("PHP Version"),
 *   description = @Translation("The currently running version of PHP.")
 * )
 */
class PhpVersion extends VitalsCheckPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return sprintf(
      '%d.%d.%d',
      PHP_MAJOR_VERSION,
      PHP_MINOR_VERSION,
      PHP_RELEASE_VERSION
    );
  }

}
