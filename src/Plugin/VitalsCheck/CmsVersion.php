<?php

namespace Drupal\vitals\Plugin\VitalsCheck;

use Drupal\vitals\VitalsCheckPluginBase;

/**
 * Plugin implementation of the vitals_check for checking the CMS version.
 *
 * @VitalsCheck(
 *   id = "cms_version",
 *   label = @Translation("CMS Version"),
 *   description = @Translation("The currently running version of Drupal.")
 * )
 */
class CmsVersion extends VitalsCheckPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return \DRUPAL::VERSION;
  }

}
