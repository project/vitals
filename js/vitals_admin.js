/**
 * @file
 * Provides customizations to the configuration screen of the vitals module.
 */

(function () {
  'use strict';

  Drupal.behaviors.vitals_admin = {
    attach: function (context, settings) {

      var copy_button = document.getElementById('js-copy-button');

      copy_button.onclick = function () {
        var token_element = document.getElementById('js-hidden-token');
        var el = document.createElement('textarea');
        el.value = token_element.value;
        el.setAttribute('readonly', '');
        el.style = {display: 'none'};
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        return false;
      };

      var generate_button = document.getElementById('js-generate-button');

      generate_button.onclick = function () {
        var confirmed = confirm(Drupal.t('WARNING: Generating a new token will invalidate the current token!!'));

        if (confirmed) {
          return true;
        }

        return false;
      };
    }
  };
}());
