CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Customization
 * How to Use This Module
 * Troubleshooting
 * FAQ
 * Future Plans
 * Maintainers


INTRODUCTION
------------

A Drupal module to provide a secure endpoint which can be consumed to monitor
site health.


REQUIREMENTS
------------

This module requires the core update module.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/docs/8/extending-drupal-8 installing-drupal-8-modules
for further information.


CONFIGURATION
-------------

No configuration is required for this module out of the box. However, some
customization options are provided that you may wish to change.

The following items can be configured by logging into to the site as a user
with the '' permission and navigating to /admin/config/services/vitals :

Generate New Token:
Upon clicking this button, a new token will be generated and stored in the
database.

Note: Generating a token will invalidate any existing tokens, which will
prevent any existing clients from accessing the endpoint.

Copy Current Token:
Clicking this button will add the current authorization token to your the
user's clipboard.

Unauthorized Action:
This field allows you to change the behavior if the endpint is used without a
valid token. The options are to send a 403 or a 404 response.


HOW TO USE THIS MODULE
----------------------

This module is not really intended to be used by the Drupal site itself, but
rather by external clients to check on the overall status of the Drupal site.

There is an install hook that will set generate an authorization token and
set the default unauthorized access behavior to send a 404 response. If you
would like to use a different access token or change the unauthorized access
behavior, you can reconfigure the module by navigating to
/admin/config/services/vitals

To see if the endpoint is working, navigate to /admin/config/services/vitals
and look for a link at the bottom of the configuration form. Click on the link
and your browser should open a new window showing you data provided by the
endpoint.


CUSTOMIZATION
-------------

This module does not define any hooks or events that can alter its behavior at
this time.


TROUBLESHOOTING
---------------

Navigate to /admin/config/services/vitals and follow the provided link to see
if the vitals endpoint is working. If it is not, click "Generate New Token",
clear the site caches, and try again.


FAQ
---

Q: How does this module work?

A: This module pulls update information from the core update module. The
   information as well as other details (PHP version, Drupal version, etc...)
   are exposed at the provided endpoint.


FUTURE PLANS
------------

The following features are slated to be implemented in the future:

 * Provide a list of disabled / uninstalled modules that should be removed
 * Check for modules that should not be in production (ie devel).


MAINTAINERS
-----------

Current maintainers:
 * Curtis Ogle (daceej) - https://www.drupal.org/u/daceej
 * Sarah Laftchiev (yodes) - https://www.drupal.org/u/yodes


Special Thanks / Credits
------------------------

Tijs De Boeck - [tijsdeboeck](https://www.drupal.org/u/tijsdeboeck) of
[openup.media](https://www.drupal.org/open-up-media) for providing the Vitals
logo.